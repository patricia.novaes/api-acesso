package br.com.novapat.acesso.clients.cliente;

import br.com.novapat.acesso.models.dto.ClienteResponse;

public class ClienteClientFallback implements ClienteClient {


    @Override
    public ClienteResponse getClienteById(String id) {
        throw new ClienteOfflineException();
    }
}
