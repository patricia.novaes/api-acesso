package br.com.novapat.acesso.clients.cliente;

import br.com.novapat.acesso.models.dto.ClienteResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "apiclienteinfraestrutura", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

        @GetMapping("v1/cliente/{cliente_id}")
        ClienteResponse getClienteById(@PathVariable String cliente_id);


}
