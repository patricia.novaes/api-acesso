package br.com.novapat.acesso.clients.porta;

import br.com.novapat.acesso.models.dto.PortaResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "apiportainfraestrutura", configuration = PortaClientConfiguration.class)
public interface PortaClient {

        @GetMapping("v1/porta/{porta_id}")
        PortaResponse getPortaById(@PathVariable String porta_id);


}
