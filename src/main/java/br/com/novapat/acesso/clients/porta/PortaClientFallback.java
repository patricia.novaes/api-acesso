package br.com.novapat.acesso.clients.porta;

import br.com.novapat.acesso.models.dto.PortaResponse;

public class PortaClientFallback implements PortaClient {

    @Override
    public PortaResponse getPortaById(String id) {
        throw new PortaOfflineException();
    }
}
