package br.com.novapat.acesso.clients.porta;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_GATEWAY, reason = "O sistema de porta está offline!")
public class PortaOfflineException extends RuntimeException {
}
