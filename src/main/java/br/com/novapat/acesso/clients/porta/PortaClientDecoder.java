package br.com.novapat.acesso.clients.porta;

import br.com.novapat.acesso.clients.InvalidAcessoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new InvalidAcessoException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}
