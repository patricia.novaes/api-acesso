package br.com.novapat.acesso.clients.cliente;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_GATEWAY, reason = "O sistema de cliente está offline!")
public class ClienteOfflineException extends RuntimeException {
}
