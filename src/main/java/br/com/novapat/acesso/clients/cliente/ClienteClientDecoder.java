package br.com.novapat.acesso.clients.cliente;

import br.com.novapat.acesso.clients.InvalidAcessoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new InvalidAcessoException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}
