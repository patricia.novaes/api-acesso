package br.com.novapat.acesso.service;


import br.com.novapat.acesso.clients.cliente.ClienteClient;
import br.com.novapat.acesso.clients.porta.PortaClient;
import br.com.novapat.acesso.models.Acesso;
import br.com.novapat.acesso.models.dto.ClienteResponse;
import br.com.novapat.acesso.models.dto.PortaResponse;
import br.com.novapat.acesso.producer.AcessoProducer;
import br.com.novapat.acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private AcessoProducer acessoProducer;

    public Acesso create(Acesso acesso) {
        ClienteResponse clienteResponse = clienteClient.getClienteById(acesso.getClienteId().toString());
        PortaResponse portaResponse = portaClient.getPortaById(acesso.getPortaId().toString());

        acesso.setClienteId(clienteResponse.getClienteId());
        acesso.setPortaId(portaResponse.getPortaId());

        return acessoRepository.save(acesso);
    }


    public Optional<Acesso> findByClienteIdAndPortaId(Long cliente_id, Long porta_id) {
        ClienteResponse clienteResponse = clienteClient.getClienteById(cliente_id.toString());
        PortaResponse portaResponse = portaClient.getPortaById(porta_id.toString());

        Optional<Acesso> acessoDoCliente = acessoRepository.findByClienteIdAndPortaId(
                clienteResponse.getClienteId(), portaResponse.getPortaId());
        if(acessoDoCliente.isPresent()){
            acessoDoCliente.get().setTemAcesso(true);
        }

        acessoProducer.enviarAoKafka(acessoDoCliente.get());

        return acessoDoCliente;
    }

    public void delete(Long cliente_id, Long porta_id) {
        ClienteResponse clienteResponse = clienteClient.getClienteById(cliente_id.toString());
        PortaResponse portaResponse = portaClient.getPortaById(porta_id.toString());

        Optional<Acesso> acessoParaDeletar = acessoRepository.findByClienteIdAndPortaId(clienteResponse.getClienteId(), portaResponse.getPortaId());

        acessoRepository.deleteById(acessoParaDeletar.get().getId());
    }
}
