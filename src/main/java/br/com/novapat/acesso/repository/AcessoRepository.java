package br.com.novapat.acesso.repository;


import br.com.novapat.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {

    Optional<Acesso> findByClienteIdAndPortaId (Long clienteId, Long portaId);

}
