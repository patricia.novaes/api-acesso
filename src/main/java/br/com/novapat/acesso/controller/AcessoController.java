package br.com.novapat.acesso.controller;


import br.com.novapat.acesso.models.Acesso;
import br.com.novapat.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping("/acesso")
    public Acesso criarAcesso(@RequestBody Acesso acesso) {
        return acessoService.create(acesso);
    }

    @GetMapping("/acesso/{cliente_id}/{porta_id}")
    public Optional<Acesso> consultarAcesso(@PathVariable("cliente_id") Long cliente_id, @PathVariable("porta_id") Long porta_id) {
        return acessoService.findByClienteIdAndPortaId(cliente_id, porta_id);
    }

    @DeleteMapping("/acesso/{cliente_id}/{porta_id}")
    public void deletarAcesso(@PathVariable("cliente_id") Long cliente_id, @PathVariable("porta_id") Long porta_id) {
        acessoService.delete(cliente_id, porta_id);
    }

}
