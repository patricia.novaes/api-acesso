package br.com.novapat.acesso.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PortaResponse {

    @JsonProperty("id")
    private Long portaId;

    public Long getPortaId() {
        return portaId;
    }

    public void setPortaId(Long portaId) {
        this.portaId = portaId;
    }
}
