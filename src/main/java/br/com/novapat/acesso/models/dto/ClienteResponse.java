package br.com.novapat.acesso.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClienteResponse {

    @JsonProperty("id")
    private Long clienteId;

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}
