package br.com.novapat.acesso.producer;

import br.com.novapat.acesso.models.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, Acesso> producer;

    public void enviarAoKafka(Acesso acesso) {
        producer.send("spec3-patricia-novaes-1", acesso);

//        for(int i = 0; i < 30; i++) {
//            producer.send("spec2-biblioteca", i, "1", livro);
//        }
    }

}
